package exercise2;

import com.sun.source.doctree.SeeTree;

import java.util.*;

public class DifferentSets {
    public static void main(String[] args) {
        List<Character> characters = List.of('A','B','Z','E','Z','L','Z');

        // Sorted Order
        Set<Character> characterSet = new TreeSet<>(characters);
        System.out.println(characterSet);

        // Insertion Order
        Set<Character> characterLinked = new LinkedHashSet<>(characters);
        System.out.println(characterLinked);

        Set<Character> characterHashed = new HashSet<>(characters);
        System.out.println(characterHashed);
    }
}
