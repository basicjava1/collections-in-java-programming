package exercise4;

import java.util.HashMap;
import java.util.Map;

public class Occurances {
    public static void main(String[] args) {
        String str = "This is a Occurance exercise";

        Map<String, Integer> occurance = new HashMap<>();
        String[] words = str.split(" ");

        Map<String , Integer> stringOccurances = new HashMap<>();

        for(String word : words){
            Integer integer = stringOccurances.get(word);
            if(integer==null)
                stringOccurances.put(word, 1);
            else
                stringOccurances.put(word, integer + 1);
        }
        System.out.println(stringOccurances);
    }
}
