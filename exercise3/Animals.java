package exercise3;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

class StringLengthComparator implements Comparator<String>{
    public int compare(String value1, String value2){
        return Integer.compare(value1.length(), value2.length());
    }
}

public class Animals {
    public static void main(String[] args) {
        Queue<String> queue = new PriorityQueue<>(
                new StringLengthComparator()
        );
        queue.addAll(List.of("Cow","Tiger","Cat"));
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
    }
}
