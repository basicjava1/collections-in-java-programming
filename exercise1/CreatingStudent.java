package exercise1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CreatingStudent {
    public static void main(String[] args) {
        List<Student> student1 = List.of(new Student("Rahul",5));
        List<Student> studentAl = new ArrayList<>(student1);
        System.out.println(student1);
    }
}
